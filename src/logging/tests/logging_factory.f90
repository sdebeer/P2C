module logging_factory
   use logging

   implicit none

   type, private :: logging_factory_t
      logical :: initialized = .false.
   contains
      procedure :: init => logging_factory_init
      procedure :: generate => logging_factory_generate
   end type logging_factory_t

   type(logging_factory_t) :: log_factory = logging_factory_t()

contains

   subroutine logging_factory_init(this)
      implicit none

      class(logging_factory_t), intent(inout) :: this

      this%initialized = .true.
   end subroutine logging_factory_init

   function logging_factory_generate(this) result(logging)
      implicit none

      class(logging_factory_t), intent(inout) :: this
      type(logging_t) :: lgr

      if (.not. this%initialized) call this%init

      call logging%log(':)')
   end function logging_factory_generate
end module logging_factory
