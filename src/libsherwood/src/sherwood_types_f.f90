module sherwood_types
   use iso_c_binding

   implicit none

   enum, bind(c)
      enumerator :: &
         gas_particles = 0, &
         DM_particles = 1, &
         star_particles = 4, &
         BH_particles = 5
   end enum

   public gas_particles, DM_particles, star_particles, BH_particles

   type, bind(C) :: sherwood_header_t
      real(c_double) :: BoxSize, HubbleParam, &
                        Omega0, OmegaLambda, Redshift, Time, MassTable(6)
      integer(c_int) :: Flag_Cooling, Flag_DoublePrecision, Flag_Feedback, &
                        Flag_IC_Info, Flag_Metals, Flag_Sfr, Flag_StellarAge, &
                        NumFilesPerSnapshot, NumPart_ThisFile(6), NumPart_Total(6), &
                        NumPart_Total_HighWord(6)
      character(len=1, kind=c_char), dimension(256) :: RunLabel = ''; 
   end type sherwood_header_t

   type, bind(C) :: sherwood_t
      type(sherwood_header_t) :: header
   end type sherwood_t
end module sherwood_types
