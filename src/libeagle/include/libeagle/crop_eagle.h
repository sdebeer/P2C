#ifndef _CROP_EAGLE_H_
#define _CROP_EAGLE_H_

#include "./eagle_types.h"
#include <hdf5.h>

int crop_eagle(eagle_hash_t *, double, double, double, double, double, double);

#endif /* _CROP_EAGLE_H_ */
