cmake_minimum_required(VERSION 3.5)

project(libeagle)
enable_language(Fortran C)

if("${CMAKE_Fortran_COMPILER_ID}" STREQUAL "Intel")
  add_compile_options(-warn all -fpp -O3)
  set(CMAKE_Fortran_FLAGS "-stand f08")
elseif("${CMAKE_Fortran_COMPILER_ID}" STREQUAL "GNU")
  add_compile_options(-Wall -Wextra -cpp -O3) # -fopt-info)
  set(CMAKE_Fortran_FLAGS "-std=f2008")
endif()

include_directories(include)

set(INCLUDE_DIR
    ${CMAKE_BINARY_DIR}/include
    CACHE PATH "Include directory")
set(CMAKE_Fortran_MODULE_DIRECTORY ${INCLUDE_DIR})

find_package(HDF5 REQUIRED COMPONENTS Fortran C)

set(deps high5)

set(deps_include)

foreach(dep ${deps})
  if(NOT TARGET ${dep})
    add_subdirectory(../${dep} ${CMAKE_BINARY_DIR}/${dep})
  endif()
  list(APPEND deps_include ../${dep}/include)
endforeach(dep)

set(srcs
    src/count_particles.c
    src/crop_eagle.c
    src/init_eagle.c
    src/init_hash.c
    src/peano_hilbert_key.c
    src/read_eagle_dset.c
    src/libeagle.f90
    src/eagle_types_f.f90)

add_library(${PROJECT_NAME} STATIC ${srcs})
target_link_libraries(${PROJECT_NAME} PRIVATE ${deps} ${HDF5_Fortran_LIBRARIES})
target_include_directories(${PROJECT_NAME} PUBLIC ${deps_include}
                                                  ${HDF5_Fortran_INCLUDE_DIRS})
