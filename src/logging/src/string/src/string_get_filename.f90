submodule(string) string_get_filename_smod
contains
pure elemental module function strin_get_filename(path) result(filename)
   implicit none

   character(len=*), intent(in) :: path
   character(len=256) :: filename

   integer :: slash_loc

   slash_loc = scan(path, '/', BACK=.true.) + 1

   filename = adjustl(trim(path(slash_loc:)))
end function strin_get_filename
end submodule string_get_filename_smod
