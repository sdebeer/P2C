logical function string_get_filename_test() result(failed)
   use string

   implicit none

   character(len=256) :: filename

   filename = strin_get_filename('/path/to/the/file.txt')
   failed = trim(filename) .ne. 'file.txt'
   if (failed) return

   filename = strin_get_filename('filename.txt')
   failed = trim(filename) .ne. 'filename.txt'
   if (failed) return

   filename = .filename.'/path/to/file'
   failed = trim(filename) .ne. 'file'
   if (failed) return
end function string_get_filename_test
