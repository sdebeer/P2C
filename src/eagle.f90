module eagle
   ! TODO: refactor this file into separated submodules
   use hdf5
   use libeagle
   use eagle_types
   use iso_c_binding
   use logging
   use plotter

   implicit none

contains
   subroutine readeagle(file_path_fmt, gas_pos, gas_vel, gas_u, gas_rho, gas_rho_entropy, &
                        gas_mass, smoothing_len, gas_metallicity, star_mass, star_pos, star_formation_t, &
                        star_metallicity, ave_baryonic_rho, redshift, expansion_factor, box_comoving_origin, &
                        box_comoving_len, sf_rho_threshold, simulation_t, gas_n, star_n, &
                        grid_n, mass2Msun, &
                        length2kpc, cut_le, cut_re, recentringBox, newOrigin, calc_emissivity, &
                        delta, logger)

      implicit none

      character(len=250), intent(in) :: file_path_fmt

      real(kind=4), allocatable, intent(out) :: gas_pos(:, :), gas_vel(:, :), gas_rho_entropy(:), &
                                                gas_u(:), gas_rho(:), gas_mass(:), smoothing_len(:), gas_metallicity(:), &
                                                star_mass(:), star_pos(:, :), star_formation_t(:), star_metallicity(:)

      real(kind=8), intent(out) :: box_comoving_len
      real(kind=4), intent(out) :: sf_rho_threshold, &
                                   simulation_t, mass2Msun, length2kpc
      real(kind=8) :: redshift, expansion_factor, box_comoving_origin(3), &
                      cut_le(3), cut_re(3), temp_le(3), temp_re(3), newOrigin(3)

      logical :: recentringBox, calc_emissivity

      integer(kind=8) :: gas_n
      integer, intent(out) :: star_n
      integer, intent(inout) :: grid_n, delta
      type(logging_t), intent(inout) :: logger

      ! Internal variables
      integer :: err
      integer(kind=8) :: counter, i, index
      integer(c_long_long), dimension(6) :: nparts
      type(eagle_t), target :: snap
      type(eagle_constants_t) :: const
      type(eagle_header_t) :: header
      type(eagle_units_t) :: units
      type(eagle_runtime_pars_t) :: runtime_pars
      type(eagle_dset_info_t), target :: dset_info
      type(eagle_hash_t), target :: hash

      ! Parameters
      real(kind=8), parameter :: m_hydrogen_cgs = 1.6735575d-24

      ! Plotter
      type(plotter_canvas_t) :: canvas
      type(plotter_histogram_t) :: coor_hist, vel_hist, hsml_hist, temp_hist, sage_hist
      integer :: hist_id
      character(len=128) :: hist_label
      character(len=1) :: vel_labels(3) = ['x', 'y', 'z']

      real(kind=4), allocatable :: gas_pos_(:, :), gas_vel_(:, :), gas_entropy_(:), gas_internalEnergy_(:), &
                                   gas_u_(:), gas_rho_(:), gas_mass_(:), smoothing_len_(:), gas_metallicity_(:), &
                                   star_mass_(:), star_pos_(:, :), star_formation_t_(:), star_metallicity_(:), &
                                   gas_rho_entropy_(:), hydrogen_abundance(:)

      ! Eagle units and constants
      real(kind=8) :: H_frac, He_frac, UnitLength_in_cm, UnitMass_in_g, &
                      UnitVelocity_in_cm_per_s, UnitTime_in_s, UnitDensity_in_cgs, &
                      UnitPressure_in_cgs, UnitEnergy_in_cgs, hubble, rho_crit_0_cgs, &
                      rho_conv_factor, ave_baryonic_rho, ave_grid_mass, dx(3), cell_volume

      call logger%begin_section('eagle')
      call canvas%init(80, 20)

      call logger%log('path', '(formatted)', '=', [file_path_fmt])
      call init_eagle_f(file_path_fmt, snap, err)

      const = snap%constants
      header = snap%header
      units = snap%units
      runtime_pars = snap%runtime_pars

      if (all(cut_le == -1.e9) .and. all(cut_re == 1.e9)) then !default values
         call logger%warn('', 'cut', '=', [cut_le, cut_re])
         cut_le = 0.0d0
         cut_re = header%BoxSize
      else
         call logger%warn('Cutting!', 'cut', '=', [cut_le, cut_re])
      end if

      call init_hash_f(file_path_fmt, hash, err)

      if (recentringBox) then
         ! to recentre the box all the particles need to be loaded
         ! so will need crop_eagle_f to take cut_le & cut_re values that encompass the whole box
         ! => initialize temporary variables
         temp_le = 0.0d0
         temp_re = header%BoxSize
         call crop_eagle_f(hash, temp_le(1), temp_le(2), temp_le(3), &
                        temp_re(1) - temp_le(1), temp_re(2) - temp_le(2), &
                        temp_re(3) - temp_le(3), err)
      else
         call crop_eagle_f(hash, cut_le(1), cut_le(2), cut_le(3), &
                        cut_re(1) - cut_le(1), cut_re(2) - cut_le(2), &
                        cut_re(3) - cut_le(3), err)
      end if

      call count_particles_f(file_path_fmt, hash, nparts, err)

      call logger%begin_section('units')

      UnitLength_in_cm = units%UnitLength_in_cm
      UnitMass_in_g = units%UnitMass_in_g
      UnitVelocity_in_cm_per_s = units%UnitVelocity_in_cm_per_s
      UnitTime_in_s = units%UnitTime_in_s
      UnitDensity_in_cgs = units%UnitDensity_in_cgs
      UnitPressure_in_cgs = units%UnitPressure_in_cgs
      UnitEnergy_in_cgs = units%UnitEnergy_in_cgs

      mass2Msun = UnitMass_in_g/const%SOLAR_MASS
      length2kpc = UnitLength_in_cm/const%CM_PER_MPC*1000

      rho_conv_factor = UnitMass_in_g/UnitLength_in_cm**3

      call logger%log('length', '[cm]', '=', [UnitLength_in_cm])
      call logger%log('length', '[kpc]', '=', [length2kpc])
      call logger%log('mass', '[g]', '=', [UnitMass_in_g])
      call logger%log('mass', '[Msun]', '=', [mass2Msun])
      call logger%log('velocity', '[cm s^-1]', '=', [UnitVelocity_in_cm_per_s])
      call logger%log('time', '[s]', '=', [UnitTime_in_s])
      call logger%log('density', '[g cm^-3]', '=', [UnitDensity_in_cgs])
      call logger%log('pressure', '[dyn cm^-2]', '=', [UnitPressure_in_cgs])
      call logger%log('energy', '[erg]', '=', [UnitEnergy_in_cgs])

      call logger%end_section ! units

      call logger%begin_section('snapshot')

      simulation_t = header%Time
      redshift = header%Redshift
      expansion_factor = header%Time
      hubble = header%HubbleParam

      H_frac = runtime_pars%InitAbundance_Hydrogen
      He_frac = runtime_pars%InitAbundance_Helium

      gas_n = int(nparts(gas_particles + 1),8) ! Stupid C and FORTRAN indexing conventions and need to specify that int()
      !returns an integer(kind=8) to be able to handle the particle from the whole box
      call logger%log('# of gas particles', '(before precise cut)', '=', [gas_n])

      star_n = int(nparts(star_particles + 1),8)
      call logger%log('# of star particles', '(before precise cut)', '=', [star_n])

      call logger%log('t', '', '=', [simulation_t])
      call logger%log('a', '', '=', [expansion_factor])
      call logger%log('z', '', '=', [redshift])
      call logger%log('H0', '', '=', [hubble])
      call logger%log('f_HI', '', '=', [H_frac])
      call logger%log('f_HeI', '', '=', [He_frac])
      call logger%log('l_box', '[ckpc]', '=', [header%BoxSize*length2kpc/hubble])

      if (grid_n == -1) then
         grid_n = int(gas_n**(1./3.))
         call logger%warn('Base grid has been changed!', 'base_grid', '=', [grid_n])
      end if

      rho_crit_0_cgs = (3.d0*(const%HUBBLE*hubble)**2)/(8.d0*const%PI*const%GRAVITY)
      call logger%log('rho_crit (z = 0)', '[g cm^-3]', '=', [rho_crit_0_cgs])

      ave_baryonic_rho = header%OmegaBaryon*rho_crit_0_cgs !
      call logger%log('rho_baryon (z = 0)', '[g cm^-3]', '=', [ave_baryonic_rho])

      if (sf_rho_threshold == -1e0) then
         sf_rho_threshold = runtime_pars%SF_THRESH_MaxPhysDens_HpCM3
         sf_rho_threshold = sf_rho_threshold/ave_baryonic_rho*m_hydrogen_cgs
         call logger%warn('Star formation overdensity threshold has been changed!', &
                          '', '=>', [sf_rho_threshold])
         call logger%warn('Star formation overdensity threshold is only effective if skipISM is true!')
      end if
      call logger%log('SFR_density_threshold', '[overdensity]', &
                      '=', [sf_rho_threshold])

      call logger%end_section ! snapshot

      call logger%begin_section('gas_particles')
      call logger%begin_section('loading')

      allocate ( &
         gas_pos_(3, gas_n), &
         gas_vel_(3, gas_n), &
         gas_u_(gas_n), &
         gas_rho_(gas_n), &
         gas_rho_entropy_(gas_n), &
         gas_mass_(gas_n), &
         smoothing_len_(gas_n), &
         gas_metallicity_(gas_n), &
         hydrogen_abundance(gas_n))

      call logger%begin_section('coordinate')

      call read_eagle_dset_2d_f(file_path_fmt, gas_particles, "Coordinates", &
                                H5T_NATIVE_REAL, gas_pos_, dset_info, hash, err)

      call logger%log('x_min', '* h / a [cMpc]', '=', [minval(gas_pos_(1, :))])
      call logger%log('x_max', '* h / a [cMpc]', '=', [maxval(gas_pos_(1, :))])
      call logger%log('y_min', '* h / a [cMpc]', '=', [minval(gas_pos_(2, :))])
      call logger%log('y_max', '* h / a [cMpc]', '=', [maxval(gas_pos_(2, :))])
      call logger%log('z_min', '* h / a [cMpc]', '=', [minval(gas_pos_(3, :))])
      call logger%log('z_max', '* h / a [cMpc]', '=', [maxval(gas_pos_(3, :))])

      call logger%log('histogram')

      coor_hist = plotter_histogram(gas_pos_, 80, plid%linear, normalized=.true.)

      call canvas%add_axis(plid%left, 6, [0e0, maxval(coor_hist%counts)], &
                           scale=plid%linear, label='Pos histogram', color=colors%indigo)
      call canvas%add_axis(plid%bottom, 8, &
                           [minval(gas_pos_), maxval(gas_pos_)], &
                           scale=plid%linear, label='Position * h / a [cMpc]', color=colors%indigo)

      call coor_hist%draw_on(canvas, xaxis=plid%bottom, yaxis=plid%left, color=colors%indigo)

      call canvas%plot
      call canvas%clear

      call logger%end_section ! coordinate

      call logger%begin_section('velocities')

      call read_eagle_dset_2d_f(file_path_fmt, gas_particles, "Velocity", &
                                H5T_NATIVE_REAL, gas_vel_, dset_info, hash, err)

      call logger%log('vx_min', '/ a^.5 [km s^-1]', '=', [minval(gas_vel_(1, :))])
      call logger%log('vx_max', '/ a^.5 [km s^-1]', '=', [maxval(gas_vel_(1, :))])
      call logger%log('vy_min', '/ a^.5 [km s^-1]', '=', [minval(gas_vel_(2, :))])
      call logger%log('vy_max', '/ a^.5 [km s^-1]', '=', [maxval(gas_vel_(2, :))])
      call logger%log('vz_min', '/ a^.5 [km s^-1]', '=', [minval(gas_vel_(3, :))])
      call logger%log('vz_max', '/ a^.5 [km s^-1]', '=', [maxval(gas_vel_(3, :))])

      call logger%log('histograms')

      do hist_id = 1, 3
         vel_hist = plotter_histogram(gas_vel_(hist_id, :), 80, plid%linear, normalized=.true.)

         write (hist_label, '(A4,A1,A)') 'Vel ', vel_labels(hist_id), ' histogram'
         call canvas%add_axis(plid%left, 6, [0e0, maxval(vel_hist%counts)], &
                              scale=plid%linear, label=hist_label, color=colors%indigo)

         write (hist_label, '(A2,A1,A)') 'v_', vel_labels(hist_id), ' / a^.5 [km s^-1]'
         call canvas%add_axis(plid%bottom, 8, &
                              [minval(gas_vel_(hist_id, :)), maxval(gas_vel_(hist_id, :))], &
                              scale=plid%linear, label=hist_label, color=colors%indigo)

         call vel_hist%draw_on(canvas, xaxis=plid%bottom, yaxis=plid%left, color=colors%indigo)

         call canvas%plot
         call canvas%clear
      end do

      call logger%end_section ! velocity

      call logger%begin_section('temperature')

      call read_eagle_dset_1d_f(file_path_fmt, gas_particles, "Temperature", &
                                H5T_NATIVE_REAL, gas_u_, dset_info, hash, err)

      call logger%log('min', '[K]', '=', [minval(gas_u_)])
      call logger%log('max', '[K]', '=', [maxval(gas_u_)])

      call logger%log('histogram')

      temp_hist = plotter_histogram(gas_u_, 80, plid%log, normalized=.true.)

      call canvas%add_axis(plid%left, 6, [0e0, maxval(temp_hist%counts)], &
                           scale=plid%linear, label='Temp histogram', color=colors%indigo)
      call canvas%add_axis(plid%bottom, 8, &
                           [minval(gas_u_), maxval(gas_u_)], &
                           scale=plid%log, label='Temperature [K]', color=colors%indigo)

      call temp_hist%draw_on(canvas, xaxis=plid%bottom, yaxis=plid%left, color=colors%indigo)

      call canvas%plot
      call canvas%clear

      call logger%end_section ! temperature

      call logger%begin_section('entropy_density')

      !.. first obtain the density via the entropy and internal energy
      allocate (gas_entropy_(gas_n), gas_internalEnergy_(gas_n))

      ! read entropy from the EAGLE snapshot into the varibale gas_entropy_ and convert into
      ! physical cgs units according to standard EAGLE procedure (see the EAGLE Team 2017)
      call read_eagle_dset_1d_f(file_path_fmt, gas_particles, "Entropy", &
          H5T_NATIVE_REAL, gas_entropy_, dset_info, hash, err)
      ! concerning the syntax: the various conversion factors and exponents can be and are read from the hdf5 files
      ! see src/libeagle/src/eagle_types_f.f90 on how they are stored and how to  access them
      gas_entropy_ = gas_entropy_ * expansion_factor**dset_info%aexp_scale_exponent * hubble**dset_info%h_scale_exponent *dset_info%CGSConversionFactor

      ! same for internal energy
      call read_eagle_dset_1d_f(file_path_fmt, gas_particles, "InternalEnergy", &
          H5T_NATIVE_REAL, gas_internalEnergy_, dset_info, hash, err)
      gas_internalEnergy_ = gas_internalEnergy_ * expansion_factor**dset_info%aexp_scale_exponent * hubble**dset_info%h_scale_exponent *dset_info%CGSConversionFactor

      ! same for hydrogen abundance
      call read_eagle_dset_1d_f(file_path_fmt, gas_particles, "ElementAbundance/Hydrogen", &
          H5T_NATIVE_REAL, hydrogen_abundance, dset_info, hash, err)
      hydrogen_abundance = hydrogen_abundance * expansion_factor**dset_info%aexp_scale_exponent * hubble**dset_info%h_scale_exponent *dset_info%CGSConversionFactor

      ! through standard definition can link internal energy and pressure PV = (2/3)U
      ! combine this with density definition using entropy (Schaye et al. 2014) P = A*rho**(5/3) and thereby derive the following definition:
      gas_rho_entropy_ = ((2./3.) * (gas_internalEnergy_/gas_entropy_))**(3./2.) ! units of gas_rho_ are now physical cgs
      !.. convert the entropy based density to the hydrogen only density by multiplying by the hydrogen abundance
      gas_rho_entropy_ = gas_rho_entropy_ * hydrogen_abundance

      deallocate(gas_entropy_, gas_internalEnergy_, hydrogen_abundance)

      call logger%log('min', '[g cm^-3]', '=', [minval(gas_rho_entropy_)])
      call logger%log('max', '[g cm^-3]', '=', [maxval(gas_rho_entropy_)])

      call logger%end_section

      call logger%begin_section('density')

      call read_eagle_dset_1d_f(file_path_fmt, gas_particles, "Density", &
          H5T_NATIVE_REAL, gas_rho_, dset_info, hash, err)

      call logger%log('min', '/ h^2 * a^3 [1e10 Msun Mpc^-3]', '=', [minval(gas_rho_)])
      call logger%log('max', '/ h^2 * a^3 [1e10 Msun Mpc^-3]', '=', [maxval(gas_rho_)])

      call logger%end_section ! density

      call logger%begin_section('mass')

      call read_eagle_dset_1d_f(file_path_fmt, gas_particles, "Mass", &
                                H5T_NATIVE_REAL, gas_mass_, dset_info, hash, err)

      call logger%log('min', '* h [1e10 Msun]', '=', [minval(gas_mass_)])
      call logger%log('max', '* h [1e10 Msun]', '=', [maxval(gas_mass_)])

      call logger%end_section ! mass

      call logger%begin_section('smoothing_length')

      call read_eagle_dset_1d_f(file_path_fmt, gas_particles, "SmoothingLength", &
                                H5T_NATIVE_REAL, smoothing_len_, dset_info, hash, err)

      call logger%log('min', '* h / a [Mpc]', '=', [minval(smoothing_len_)])
      call logger%log('max', '* h / a [Mpc]', '=', [maxval(smoothing_len_)])

      call logger%log('histogram')

      hsml_hist = plotter_histogram(smoothing_len_, 80, plid%log, normalized=.true.)

      call canvas%add_axis(plid%left, 6, [0e0, maxval(hsml_hist%counts)], &
                           scale=plid%linear, label='SML histogram', color=colors%indigo)
      call canvas%add_axis(plid%bottom, 8, &
                           [minval(smoothing_len_), maxval(smoothing_len_)], &
                           scale=plid%log, label='Smoothing length * h / a [Mpc]', color=colors%indigo)

      call hsml_hist%draw_on(canvas, xaxis=plid%bottom, yaxis=plid%left, color=colors%indigo)

      call canvas%plot
      call canvas%clear

      call logger%end_section ! smoothing_length

      call logger%begin_section('metallicity')

      call read_eagle_dset_1d_f(file_path_fmt, gas_particles, "Metallicity", &
                                H5T_NATIVE_REAL, gas_metallicity_, dset_info, hash, err)

      call logger%log('min', '[(m_i)/M]', '=', [minval(gas_metallicity_)])
      call logger%log('max', '[(m_i)/M]', '=', [maxval(gas_metallicity_)])

      call logger%end_section ! metallicity

      call logger%end_section ! loading

      call logger%begin_section('precise_cut')

      counter = 1
      do i = 1, gas_n
         if(recentringBox) then
            ! subtract new origin from the Coordinates individually
            gas_pos_(:,i) = gas_pos_(:,i) - newOrigin
            ! for each axis, check whether the subtraction takes the coordinate
            ! out of bounds of [0,header%BoxSize], if so: transform it into bounds.
            do index=1,3
               if(gas_pos_(index,i) < 0.0 )then
                  gas_pos_(index,i) = header%BoxSize + gas_pos_(index,i)
               else if(header%BoxSize < gas_pos_(index,i) ) then
                  gas_pos_(index,i) = gas_pos_(index,i) - header%BoxSize
               end if
            end do
         end if

         if (all(gas_pos_(:, i) >= cut_le) .and. all(gas_pos_(:, i) <= cut_re)) then
            if (counter < i) then
               gas_pos_(:, counter) = gas_pos_(:, i)
               gas_vel_(:, counter) = gas_vel_(:, i)
               gas_rho_(counter) = gas_rho_(i)
               gas_rho_entropy_(counter) = gas_rho_entropy_(i)
               gas_u_(counter) = gas_u_(i)
               gas_mass_(counter) = gas_mass_(i)
               smoothing_len_(counter) = smoothing_len_(i)
               gas_metallicity_(counter) = gas_metallicity_(i)
            end if
            counter = counter + 1
         end if
      end do

      call logger%log('# of removed gas particles', '', '=', [gas_n - counter + 1])
      gas_n = counter - 1
      call logger%log('# of gas particles', '', '=', [gas_n])

      allocate (gas_pos(3, gas_n), gas_vel(3, gas_n), gas_u(gas_n), gas_rho(gas_n), &
                gas_rho_entropy(gas_n), gas_mass(gas_n), smoothing_len(gas_n), gas_metallicity(gas_n))

      gas_pos(:, :) = gas_pos_(:, 1:gas_n)
      gas_vel(:, :) = gas_vel_(:, 1:gas_n)
      gas_u(:) = gas_u_(1:gas_n)
      gas_rho(:) = gas_rho_(1:gas_n)
      gas_rho_entropy(:) = gas_rho_entropy_(1:gas_n)
      gas_mass(:) = gas_mass_(1:gas_n)
      smoothing_len(:) = smoothing_len_(1:gas_n)
      gas_metallicity(:) = gas_metallicity_(1:gas_n)

      do i = 1, 3
         dx(i) = maxval(gas_pos(i, :)) - minval(gas_pos(i, :))
      end do

      box_comoving_len = maxval(dx)/hubble
      call logger%log('l_box', '[ckpc]', '=', [box_comoving_len*length2kpc])

      do i = 1, 3
         box_comoving_origin(i) = minval(gas_pos(i, :))/hubble
      end do

      call logger%log('o_box', '[ckpc]', '=', box_comoving_origin(1:3)*length2kpc)

      call logger%log('x_min', '[ckpc]', '=', [minval(gas_pos(1, :))*length2kpc/hubble])
      call logger%log('x_max', '[ckpc]', '=', [maxval(gas_pos(1, :))*length2kpc/hubble])
      call logger%log('y_min', '[ckpc]', '=', [minval(gas_pos(2, :))*length2kpc/hubble])
      call logger%log('y_max', '[ckpc]', '=', [maxval(gas_pos(2, :))*length2kpc/hubble])
      call logger%log('z_min', '[ckpc]', '=', [minval(gas_pos(3, :))*length2kpc/hubble])
      call logger%log('z_max', '[ckpc]', '=', [maxval(gas_pos(3, :))*length2kpc/hubble])

      do i = 1, gas_n
         gas_pos(:, i) = (gas_pos(:, i) - (box_comoving_origin*hubble))/(box_comoving_len*hubble)
      end do

      where (gas_pos == 0.) gas_pos = nearest(gas_pos, 1.d0)
      where (gas_pos == 1.) gas_pos = nearest(gas_pos, -1.d0)

      call logger%log('X (min, max)', '[normalized]', '=', [minval(gas_pos), maxval(gas_pos)])

      call logger%log('vx_min', '/ a^.5 [km s^-1]', '=', [minval(gas_vel(1, :))])
      call logger%log('vx_max', '/ a^.5 [km s^-1]', '=', [maxval(gas_vel(1, :))])
      call logger%log('vy_min', '/ a^.5 [km s^-1]', '=', [minval(gas_vel(2, :))])
      call logger%log('vy_max', '/ a^.5 [km s^-1]', '=', [maxval(gas_vel(2, :))])
      call logger%log('vz_min', '/ a^.5 [km s^-1]', '=', [minval(gas_vel(3, :))])
      call logger%log('vz_max', '/ a^.5 [km s^-1]', '=', [maxval(gas_vel(3, :))])

      cell_volume = (box_comoving_len*UnitLength_in_cm)**3/grid_n**3 !.. units: cm^3
      ave_grid_mass = cell_volume * ave_baryonic_rho
      !ave_grid_mass = (box_comoving_len*UnitLength_in_cm)**3/grid_n**3 &
                      !*ave_baryonic_rho
      call logger%log('<grid_mass>', '[g]', '=', [ave_grid_mass])

      !.. due to the way gas_rho_entropy is calculated using the entropy based density defnition
      !.. (see line 263 & Schaye et al. 2014) the units of gas_rho_entropy are physical cgs at this point in the code
      !.. -> need to be converted to co-moving cgs and then divided by ave_baryonic_rho to obtain units of overdensity
      gas_rho_entropy = (gas_rho_entropy * expansion_factor**3)/ave_baryonic_rho

      call logger%log('delta_min_entropy', '(over_density)', '=', [minval(gas_rho_entropy)])
      call logger%log('delta_max_entropy', '(over_density)', '=', [maxval(gas_rho_entropy)])

      !.. See: The eagle simulations of galaxy formation: Public release of particle data:
      !.. the units of gas_rho as read from the snapshot are: h**2 * comoving(10^10Msol/Mpc^3)
      !.. gas_rho in comoving g/cm^3: gas_rho = gas_rho * rho_conv_factor * hubble**2
      !.. units of ave_baryonic_rho: comoving g/cm^3
      gas_rho = (gas_rho * rho_conv_factor * hubble**2)/ave_baryonic_rho
      !.. units of gas_rho and gas_rho_entropy are now overdensity

      call logger%log('delta_min', '(over_density)', '=', [minval(gas_rho)])
      call logger%log('delta_max', '(over_density)', '=', [maxval(gas_rho)])

      call logger%log('T_min', '[K]', '=', [minval(gas_u)])
      call logger%log('T_max', '[K]', '=', [maxval(gas_u)])

      delta = 0 ! we want to refine the entire box in eagle.

      ! Gas particle masses
      gas_mass = (gas_mass*UnitMass_in_g/hubble)/ave_grid_mass
      call logger%log('M_min', '[normalized by <grid_mass>]', '=', [minval(gas_mass)])
      call logger%log('M_max', '[normalized by <grid_mass>]', '=', [maxval(gas_mass)])

      smoothing_len = smoothing_len/(box_comoving_len*hubble)

      call logger%end_section(print_duration=.true.) ! precise cut
      call logger%end_section ! gas particles

      if (star_n > 0 .and. .not. calc_emissivity) then
         call logger%begin_section('star_particles')
         call logger%begin_section('loading')

         allocate ( &
            star_pos_(3, star_n), &
            star_formation_t_(star_n), &
            star_mass_(star_n), &
            star_metallicity_(star_n))

         call logger%begin_section('coordinate')

         call read_eagle_dset_2d_f(file_path_fmt, star_particles, "Coordinates", &
                                   H5T_NATIVE_REAL, star_pos_, dset_info, hash, err)

         call logger%log('histogram')

         coor_hist = plotter_histogram(star_pos_, 80, plid%linear, normalized=.true.)

         call canvas%add_axis(plid%left, 6, [0e0, maxval(coor_hist%counts)], &
                              scale=plid%linear, label='Pos histogram', color=colors%indigo)
         call canvas%add_axis(plid%bottom, 8, &
                              [minval(star_pos_), maxval(star_pos_)], &
                              scale=plid%linear, label='Position * h / a [cMpc]', color=colors%indigo)

         call coor_hist%draw_on(canvas, xaxis=plid%bottom, yaxis=plid%left, color=colors%indigo)

         call canvas%plot
         call canvas%clear

         call logger%end_section ! coordinate

         call logger%begin_section('star_formation_time')

         call read_eagle_dset_1d_f(file_path_fmt, star_particles, &
                                   "StellarFormationTime", H5T_NATIVE_REAL, &
                                   star_formation_t_, dset_info, hash, err)

         call logger%log('histogram')

         sage_hist = plotter_histogram(star_formation_t_, 80, plid%linear, normalized=.true.)

         call canvas%add_axis(plid%left, 6, [0e0, maxval(sage_hist%counts)], &
                              scale=plid%linear, label='Star birth histogram', color=colors%indigo)
         call canvas%add_axis(plid%bottom, 8, &
                              [minval(star_formation_t_), maxval(star_formation_t_)], &
                              scale=plid%linear, label='Stellar formation time [in expansion factor]', color=colors%indigo)

         call sage_hist%draw_on(canvas, xaxis=plid%bottom, yaxis=plid%left, color=colors%indigo)

         call canvas%plot
         call canvas%clear

         call logger%end_section ! star_formation_time

         call logger%begin_section('mass')

         call read_eagle_dset_1d_f(file_path_fmt, star_particles, "Mass", &
                                   H5T_NATIVE_REAL, star_mass_, dset_info, hash, err)

         call logger%end_section ! mass

         call logger%begin_section('metallicity')

         call read_eagle_dset_1d_f(file_path_fmt, star_particles, "Metallicity", &
                                   H5T_NATIVE_REAL, star_metallicity_, dset_info, hash, err)

         call logger%end_section ! metallicity

         call logger%end_section ! loading

         call logger%begin_section('precise_cut')

         counter = 1
         do i = 1, star_n
            if (all(star_pos_(:, i) >= cut_le) .and. all(star_pos_(:, i) <= cut_re)) then
               if (counter < i) then
                  star_pos_(:, counter) = star_pos_(:, i)
                  star_formation_t_(counter) = star_formation_t_(i)
                  star_mass_(counter) = star_mass_(i)
                  star_metallicity_(counter) = star_metallicity_(i)
               end if
               counter = counter + 1
            end if
         end do

         call logger%log('# of removed star particles', '', '=', [star_n - counter + 1])
         star_n = counter - 1
         call logger%log('# of star particles', '', '=', [star_n])

         allocate (star_pos(3, star_n), star_formation_t(star_n), &
                   star_mass(star_n), star_metallicity(star_n))

         star_pos(:, :) = star_pos_(:, 1:star_n)
         star_formation_t(:) = star_formation_t_(1:star_n)
         star_mass(:) = star_mass_(1:star_n)
         star_metallicity(:) = star_metallicity_(1:star_n)

         do i = 1, star_n
            star_pos(:, i) = (star_pos(:, i) - (box_comoving_origin*hubble))/(box_comoving_len*hubble)
         end do

         WHERE (star_pos == 0.) star_pos = NEAREST(star_pos, 1.d0)
         WHERE (star_pos == 1.) star_pos = NEAREST(star_pos, -1.d0)

         call logger%log('x_min', '[ckpc]', '=', [minval(star_pos(1, :))*length2kpc/hubble])
         call logger%log('x_max', '[ckpc]', '=', [maxval(star_pos(1, :))*length2kpc/hubble])
         call logger%log('y_min', '[ckpc]', '=', [minval(star_pos(2, :))*length2kpc/hubble])
         call logger%log('y_max', '[ckpc]', '=', [maxval(star_pos(2, :))*length2kpc/hubble])
         call logger%log('z_min', '[ckpc]', '=', [minval(star_pos(3, :))*length2kpc/hubble])
         call logger%log('z_max', '[ckpc]', '=', [maxval(star_pos(3, :))*length2kpc/hubble])

         call logger%log('X (min, max)', '[normalized]', '=', [minval(star_pos), maxval(star_pos)])

         call logger%log('formation_time_min', '[expansion factor]', '=', [minval(star_formation_t)])
         call logger%log('formation_time_max', '[expansion factor]', '=', [maxval(star_formation_t)])

         call logger%log('M_min', ' * 1e10 [Msun]', '=', [minval(star_mass)])
         call logger%log('M_max', ' * 1e10 [Msun]', '=', [maxval(star_mass)])

         star_mass = (star_mass*UnitMass_in_g/hubble)/ave_grid_mass
         call logger%log('M_min', '[normalized]', '=', [minval(star_mass)])
         call logger%log('M_max', '[normalized]', '=', [maxval(star_mass)])

         call logger%log('Z_min', '[Zsun]', '=', [minval(star_metallicity)])
         call logger%log('Z_max', '[Zsun]', '=', [maxval(star_metallicity)])

         call logger%end_section(print_duration=.true.) ! precise cut
         call logger%end_section(print_duration=.true.) ! star particles
      end if

      call logger%end_section(print_duration=.true.) ! eagle
   end subroutine readeagle
end module eagle
