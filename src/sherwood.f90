module SHERWOOD

   use hdf5
   use libsherwood
   use sherwood_types
   use iso_c_binding

   implicit none

contains
   subroutine readsherwood(file_path_fmt, gas_pos, gas_vel, gas_u, gas_rho, &
                           gas_mass, smoothing_len, star_mass, star_pos, star_formation_t, &
                           star_metallicity, redshift, box_comoving_origin, &
                           box_comoving_len, box_comoving_len_original, sf_rho_threshold, simulation_t, gas_n, star_n, &
                           grid_n, dMSolUnit, &
                           dKpcUnit, cut_le, cut_re, delta, hubble, conv_star_gas)

      implicit none

      character(len=250), intent(in) :: file_path_fmt

      integer :: i, counter, k

      real(kind=4), allocatable, intent(out) :: gas_pos(:, :), gas_vel(:, :), &
                                                gas_u(:), gas_rho(:), gas_mass(:), smoothing_len(:), star_mass(:), &
                                                star_pos(:, :), star_formation_t(:), star_metallicity(:)
      real(kind=4), allocatable :: electron_abundance(:)

      real(kind=8), intent(out) :: box_comoving_len, box_comoving_len_original
      real(kind=4), intent(inout) :: sf_rho_threshold, &
                                     simulation_t, dMSolUnit, dKpcUnit
      real(kind=8) :: redshift, expansion_factor, box_comoving_origin(3), &
                      cut_le(3), cut_re(3)

      integer(kind=8) :: gas_n, total_particules
      integer, intent(out) :: star_n
      integer, intent(inout) :: grid_n, delta

      real(kind=8) :: dx(3)
      logical :: add_strip, conv_star_gas

      ! Internal variables
      real(kind=8) :: H_frac, He_frac
     real(kind=8) :: UnitLength_in_cm, UnitMass_in_g, UnitDensity_in_cgs, UnitTime_in_s, UnitEnergy_in_cgs, UnitVelocity_in_cm_per_s
      real(kind=4), intent(inout) :: hubble
      real(kind=8) :: rho_conv_factor, rho_crit_0_cgs, ave_baryonic_rho, ave_grid_mass, gamma_
      real(kind=4), allocatable :: gas_pos_(:, :), gas_vel_(:, :), &
                                   gas_u_(:), gas_rho_(:), gas_mass_(:), smoothing_len_(:), star_mass_(:), &
                                   star_pos_(:, :), star_formation_t_(:), star_metallicity_(:), electron_abundance_(:)
      real(kind=4), allocatable :: gas_pos_temp(:, :), gas_vel_temp(:, :), &
                                   gas_u_temp(:), gas_rho_temp(:), gas_mass_temp(:), smoothing_len_temp(:)

      real(kind=4) :: max_smoothing_len_allbox

      integer :: err
      type(sherwood_t), target :: snap
      type(sherwood_header_t) :: header

 real(kind=8) :: PI, OmegaBaryon, GRAVITY, HUBBLE_, BOLTZMANN_CGS, conversion_in_T, m_unitary, M_PROTON_SI, M_PROTON_CGS, MeanWeight

      ! Open Sherwood snapshot and load its header
      call init_sherwood_f(file_path_fmt, snap, err)

      header = snap%header

      if (all(cut_le == -1.e9) .and. all(cut_re == 1.e9)) then !default values
         cut_le = 0.0d0
         cut_re = header%BoxSize
      end if

      gas_n = header%NumPart_Total(gas_particles + 1)
      star_n = header%NumPart_Total(star_particles + 1)
      ! Size of the base grid

      if (grid_n == -1) then
         grid_n = int(gas_n**(1./3.))
         print *, "Base grid size has been changed to ", grid_n
      end if

      simulation_t = header%Time
      redshift = header%Redshift
      expansion_factor = header%Time
      hubble = header%HubbleParam
      box_comoving_len_original = header%BoxSize

      print *, "Simulation Time", simulation_t
      print *, "Redshift", redshift
      print *, "Expansion factor", expansion_factor
      print *, "Hubble constant", hubble*100

      H_frac = 0.76
      PI = 3.14159265
      OmegaBaryon = 0.0482519
      GRAVITY = 6.672e-8
      HUBBLE_ = 3.2407789e-18
      M_PROTON_SI = 1.67262e-27
      M_PROTON_CGS = 1.6726e-24
      BOLTZMANN_CGS = 1.380649e-16 ! in cgs

      print *, 'H_frac', H_frac
      print *, 'OmegaBaryon', OmegaBaryon
      print *, 'Gravitational constant', GRAVITY

      UnitLength_in_cm = 3.086d+21
      UnitMass_in_g = 1.989d+43
      UnitDensity_in_cgs = UnitMass_in_g/UnitLength_in_cm**3
      UnitVelocity_in_cm_per_s = 1.0e5
      UnitTime_in_s = UnitLength_in_cm/UnitVelocity_in_cm_per_s
      UnitEnergy_in_cgs = UnitMass_in_g*UnitLength_in_cm**2/UnitTime_in_s**2

      print *, "UnitLength_in_cm", UnitLength_in_cm
      print *, "UnitDensity_in_cgs", UnitDensity_in_cgs
      print *, "UnitMass_in_g", UnitMass_in_g

      ! Based on Sherwood simulation suit interna units
      dMSolUnit = 1d10
      dKpcUnit = 1d0

      print *, "dMSolUnit", dMSolUnit
      print *, "dKpcUnit", dKpcUnit

      print *, "Allocationg temp arrays"
      allocate (gas_pos_(3, gas_n), gas_vel_(3, gas_n), gas_u_(gas_n), &
                gas_rho_(gas_n), gas_mass_(gas_n), smoothing_len_(gas_n), electron_abundance_(gas_n))

      print *, "Reading Coordinates ..."
      call read_sherwood_dset_2d_f(file_path_fmt, gas_particles, "Coordinates", &
                                   H5T_NATIVE_REAL, gas_pos_, err)
      print *, "Reading velocities ..."
      call read_sherwood_dset_2d_f(file_path_fmt, gas_particles, "Velocities", &
                                   H5T_NATIVE_REAL, gas_vel_, err)
      print *, "Reading Density ..."
      call read_sherwood_dset_1d_f(file_path_fmt, gas_particles, "Density", &
                                   H5T_NATIVE_REAL, gas_rho_, err)
      print *, "Reading InternalEnergy ..."
      call read_sherwood_dset_1d_f(file_path_fmt, gas_particles, "InternalEnergy", &
                                   H5T_NATIVE_REAL, gas_u_, err)
      print *, "Reading Masses ..."
      call read_sherwood_dset_1d_f(file_path_fmt, gas_particles, "Masses", &
                                   H5T_NATIVE_REAL, gas_mass_, err)
      print *, "Reading SmoothingLength ..."
      call read_sherwood_dset_1d_f(file_path_fmt, gas_particles, "SmoothingLength", &
                                   H5T_NATIVE_REAL, smoothing_len_, err)
      print *, "Reading Electron abundance ..."
      call read_sherwood_dset_1d_f(file_path_fmt, gas_particles, "ElectronAbundance", &
                                   H5T_NATIVE_REAL, electron_abundance_, err)

      max_smoothing_len_allbox = maxval(smoothing_len_)
      add_strip = .true.

      cut_re = cut_re + max_smoothing_len_allbox
      cut_le = cut_le - max_smoothing_len_allbox
      do k = 1, 3
         if (cut_re(k) .GE. header%BoxSize) then
            cut_re(k) = header%BoxSize
            cut_le(k) = cut_le(k) - max_smoothing_len_allbox
         end if
         if (cut_le(k) .LE. 0) then
            cut_le(k) = 0
            cut_re(k) = cut_re(k) + max_smoothing_len_allbox
         end if
         if (cut_le(k) .LE. 0 .and. cut_re(k) .GE. header%BoxSize) then
            cut_le(k) = 0
            cut_re(k) = header%BoxSize
            add_strip = .false.
         end if
      end do

      print *, "True cut_re =", cut_re
      print *, "True cut_le =", cut_le

      ! Applying box cut
      counter = 1

      if (add_strip .eqv. .true.) then
         print *, "We added ", max_smoothing_len_allbox, "(cKpc) to the edge to solve boundary issues. Cells within added strips will &
            not be refined"
      end if

      do i = 1, gas_n
         if (all(gas_pos_(:, i) >= cut_le) .and. all(gas_pos_(:, i) <= cut_re)) then
            if (counter < i) then
               gas_pos_(:, counter) = gas_pos_(:, i)
               gas_vel_(:, counter) = gas_vel_(:, i)
               gas_rho_(counter) = gas_rho_(i)
               gas_u_(counter) = gas_u_(i)
               gas_mass_(counter) = gas_mass_(i)
               smoothing_len_(counter) = smoothing_len_(i)
               electron_abundance_(counter) = electron_abundance_(i)
            end if
            counter = counter + 1
         end if
      end do

      gas_n = counter - 1
      print *, "- Total number of gas particles (after cut)=", gas_n
      allocate (gas_pos_temp(3, gas_n), gas_vel_temp(3, gas_n), gas_u_temp(gas_n), &
                gas_rho_temp(gas_n), gas_mass_temp(gas_n), smoothing_len_temp(gas_n), electron_abundance(gas_n))

      gas_pos_temp(:, :) = gas_pos_(:, 1:gas_n)
      gas_vel_temp(:, :) = gas_vel_(:, 1:gas_n)
      gas_u_temp(:) = gas_u_(1:gas_n)
      gas_rho_temp(:) = gas_rho_(1:gas_n)
      gas_mass_temp(:) = gas_mass_(1:gas_n)
      smoothing_len_temp(:) = smoothing_len_(1:gas_n)
      electron_abundance(:) = electron_abundance_(1:gas_n)

      deallocate (gas_pos_, gas_vel_, gas_u_, gas_rho_, gas_mass_, smoothing_len_, electron_abundance_)

      do i = 1, 3
         dx(i) = maxval(gas_pos_temp(i, :)) - minval(gas_pos_temp(i, :))
      end do

      box_comoving_len = maxval(dx)/hubble

      do i = 1, 3
         box_comoving_origin(i) = minval(gas_pos_temp(i, :))/hubble
      end do

      print *, "-- Box length (cKpc)", box_comoving_len
      print *, "-- Box origin (cKpc)", box_comoving_origin(1:3)

      print *, "---------------------- Gas particles ---------------------- "
      ! Normalizing particle positions by the box size
      print *, "-- X Positions (cKpc):    min=", minval(gas_pos_temp(1, :))/hubble, &
         "max=", maxval(gas_pos_temp(1, :))/hubble
      print *, "-- y Positions (cKpc):    min=", minval(gas_pos_temp(2, :))/hubble, &
         "max=", maxval(gas_pos_temp(2, :))/hubble
      print *, "-- Z Positions (cKpc):    min=", minval(gas_pos_temp(3, :))/hubble, &
         "max=", maxval(gas_pos_temp(3, :))/hubble
      do i = 1, gas_n
         gas_pos_temp(:, i) = (gas_pos_temp(:, i) - (box_comoving_origin*hubble)) &
                              /(box_comoving_len*hubble)
      end do

      where (gas_pos_temp == 0.) gas_pos_temp = nearest(gas_pos_temp, 1.d0)
      where (gas_pos_temp == 1.) gas_pos_temp = nearest(gas_pos_temp, -1.d0)

      print *, "-- X Positions (BoxUnit): min=", minval(gas_pos_temp(1, :))/hubble, &
         "max=", maxval(gas_pos_temp(1, :))
      print *, "-- y Positions (BoxUnit): min=", minval(gas_pos_temp(2, :))/hubble, &
         "max=", maxval(gas_pos_temp(2, :))
      print *, "-- Z Positions (BoxUnit): min=", minval(gas_pos_temp(3, :))/hubble, &
         "max=", maxval(gas_pos_temp(3, :))

      ! Gas particles velocities (km s^-1)
      print *, "-- Velocity (km s^-1):    min=", minval(gas_vel_temp), "max=", maxval(gas_vel_temp)

      ! Comoving critical density in cgs
      rho_crit_0_cgs = (3.d0*(HUBBLE_*hubble)**2) &
                       /(8.d0*PI*GRAVITY)

      ! Average baryonic density in cgs
      ave_baryonic_rho = OmegaBaryon*rho_crit_0_cgs !
      if (sf_rho_threshold == -1e0) then
         sf_rho_threshold = 1000 !* ave_baryonic_rho
         print *, 'Star formation overdensity threshold has been changed! =>', sf_rho_threshold
      end if

      ! Conversion factor to cgs
      rho_conv_factor = UnitMass_in_g/UnitLength_in_cm**3

      gas_rho_temp = gas_rho_temp*rho_conv_factor/ave_baryonic_rho
      print *, "-- Density contrast:      min=", minval(gas_rho_temp), &
         "max=", maxval(gas_rho_temp)
      print *, "-- Rho_crit (cgs) =", rho_crit_0_cgs
      print *, "-- Average baryonic density (cgs) =", ave_baryonic_rho
      print *, "-- Density conversion factor to cgs=", rho_conv_factor

      ! Average mass of grid points

      max_smoothing_len_allbox = max_smoothing_len_allbox/(box_comoving_len*hubble)
      delta = INT(grid_n*max_smoothing_len_allbox) + 1
      print *, "Size of the region which will not be refined (in cell unit) ", delta

      ave_grid_mass = (box_comoving_len*UnitLength_in_cm)**3/grid_n**3 &
                      *ave_baryonic_rho

      ! Gas particle masses
      gas_mass_temp = (gas_mass_temp*UnitMass_in_g/hubble)/ave_grid_mass
      print *, "-- Mass (normalized by grid points mass): min=", &
         minval(gas_mass_temp), "max=", maxval(gas_mass_temp)
      print *, "-- Grid points mass (cgs)=", ave_grid_mass

      ! Normalizing smoothing length by the box size
      smoothing_len_temp = smoothing_len_temp/(box_comoving_len*hubble)

      ! Internal Energy (km/s)^2

      gas_u_temp = gas_u_temp*UnitEnergy_in_cgs/UnitMass_in_g
      gamma_ = 5.0/3
      gas_u_temp = 4.0/(3*H_frac + 1 + 4*H_frac*electron_abundance)*M_PROTON_CGS/BOLTZMANN_CGS*(gamma_ - 1)*gas_u_temp

      print *, "---------------------- Star particles ---------------------- :"
      if (star_n > 0) then
         allocate (star_pos_(3, star_n), star_mass_(star_n), &
                   star_formation_t_(star_n), star_metallicity_(star_n))

         ! Loading star particle datasets
         call read_sherwood_dset_2d_f(file_path_fmt, star_particles, "Coordinates", &
                                      H5T_NATIVE_REAL, star_pos_, err)
         call read_sherwood_dset_1d_f(file_path_fmt, star_particles, "Masses", &
                                      H5T_NATIVE_REAL, star_mass_, err)

         ! Applying (precise) box cut
         counter = 1

         do i = 1, star_n
            if (all(star_pos_(:, i) >= cut_le) .and. all(star_pos_(:, i) <= cut_re)) then
               if (counter < i) then
                  star_pos_(:, counter) = star_pos_(:, i)
                  star_mass_(counter) = star_mass_(i)
               end if
               counter = counter + 1
            end if
         end do

         star_n = counter - 1
         print *, "-- Total number of star particles=", star_n

         allocate (star_pos(3, star_n), star_formation_t(star_n), &
                   star_mass(star_n), star_metallicity(star_n))

         star_formation_t(star_n) = 0e0
         star_metallicity(star_n) = 0e0
         star_pos(:, :) = star_pos_(:, 1:star_n)
         star_mass(:) = star_mass_(1:star_n)

         deallocate (star_formation_t_, star_pos_, star_metallicity_, star_mass_)
         print *, "-- Total number of star particles=", star_n

         ! Star positions (Normalized by the box size)
         print *, "-- X Positions (cKpc): min=", minval(star_pos(1, :))/hubble, &
            "max=", maxval(star_pos(1, :))/hubble
         print *, "-- Y Positions (cKpc): min=", minval(star_pos(2, :))/hubble, &
            "max=", maxval(star_pos(2, :))/hubble
         print *, "-- Z Positions (cKpc): min=", minval(star_pos(3, :))/hubble, &
            "max=", maxval(star_pos(3, :))/hubble

         do i = 1, star_n
            star_pos(:, i) = (star_pos(:, i) - (box_comoving_origin*hubble)) &
                             /(box_comoving_len*hubble)
         end do

         WHERE (star_pos == 0.) star_pos = NEAREST(star_pos, 1.d0)
         WHERE (star_pos == 1.) star_pos = NEAREST(star_pos, -1.d0)
         print *, "-- Positions (normalized): min=", minval(star_pos), &
            "max=", maxval(star_pos)

         ! Star masses (10^10 M_sun)
         print *, "-- Mass (M_sun): min=", minval(star_mass)*1e10, &
            "max=", maxval(star_mass)*1e10
         star_mass = (star_mass*UnitMass_in_g/hubble)/ave_grid_mass
         print *, "-- Mass (normalized by grid points mass): min=", &
            minval(star_mass), "max=", maxval(star_mass)
      end if
      if (conv_star_gas .eqv. .true.) then
         total_particules = gas_n + star_n
         allocate (gas_pos(3, total_particules), gas_vel(3, total_particules), gas_u(total_particules), &
                   gas_rho(total_particules), gas_mass(total_particules), smoothing_len(total_particules))
         gas_pos(:, 1:gas_n) = gas_pos_temp(:, :)
         gas_pos(:, gas_n + 1:) = star_pos(:, :)
         gas_vel(:, 1:gas_n) = gas_vel_temp(:, :)
         gas_vel(:, gas_n + 1:) = 0 !we don't care about velocities
         gas_u(1:gas_n) = gas_u_temp(:)
         gas_u(gas_n + 1:) = 1e3 !temperature will anyway be fixed to 1e3K in Radamesh
         gas_rho(1:gas_n) = gas_rho_temp(:)
         gas_rho(gas_n + 1:) = 1000
         gas_mass(1:gas_n) = gas_mass_temp(:)
         gas_mass(gas_n + 1:) = star_mass(:)
         smoothing_len(1:gas_n) = smoothing_len_temp(:)
         smoothing_len(gas_n + 1:) = 3.5/(box_comoving_len*hubble)
         print *, 'successfully added star particles to gas particles'
         print *, 'number of gas particles : ', total_particules
         star_n = 0 
         gas_n = total_particules
      else
         allocate (gas_pos(3, gas_n), gas_vel(3, gas_n), gas_u(gas_n), &
                   gas_rho(gas_n), gas_mass(gas_n), smoothing_len(gas_n))
         gas_pos(:, :) = gas_pos_temp(:, :)
         gas_vel(:, :) = gas_vel_temp(:, :)
         gas_u(:) = gas_u_temp(:)
         gas_rho(:) = gas_rho_temp(:)
         gas_mass(:) = gas_mass_temp(:)
         smoothing_len(:) = smoothing_len_temp(:)
      endif

   end subroutine readsherwood
end module sherwood
