#ifndef _INIT_EAGLE_H_
#define _INIT_EAGLE_H_

#include "./eagle_types.h"
#include <hdf5.h>

hid_t init_eagle(char *, eagle_t *);

#endif /* _INIT_EAGLE_H_ */
