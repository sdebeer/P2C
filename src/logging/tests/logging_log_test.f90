logical function logging_log_test() result(failed)
   use logging

   implicit none

   type(logging_t) :: lgr

   call lgr%init('log')
   call lgr%begin_section('test')
   call lgr%log('message')
   call lgr%log('message', 'key')
   call lgr%log('message', 123)
   call lgr%log('message', 1.2e3)
   call lgr%log('message', 1.2d3)
   call lgr%begin_section('scalar')
   call lgr%log('message', 'key', 'op')
   call lgr%log('message', 'key', 'op', ['value'])
   call lgr%log('message', 'key', 'op', [123])
   call lgr%log('message', 'key', 'op', [1.2e3])
   call lgr%log('message', 'key', 'op', [1.2d3])
   call lgr%begin_section('array')
   call lgr%log('message', 'key', 'op', ['value 1', 'value 2'])
   call lgr%log('message', 'key', 'op', [123, 234])
   call lgr%log('message', 'key', 'op', [1.2e3, 2.3e4])
   call lgr%log('message', 'key', 'op', [1.2d3, 2.3d4])
   call lgr%begin_section('')
   call lgr%log('done')
   call lgr%end_section
   call lgr%end_section
   call lgr%end_section
   call lgr%end_section

   ! To see the output set failed to .true.
   failed = .false.
end function logging_log_test
