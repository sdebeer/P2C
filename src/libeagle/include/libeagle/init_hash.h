#ifndef _INIT_HASH_H_
#define _INIT_HASH_H_

#include "./eagle_types.h"
#include <hdf5.h>

int init_hash(char *, eagle_hash_t *);

#endif /* _INIT_HASH_H_ */
