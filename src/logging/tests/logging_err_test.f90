logical function logging_err_test() result(failed)
   use logging

   implicit none

   type(logging_t) :: lgr

   call lgr%init('err')
   call lgr%begin_section('test')
   call lgr%err('message')
   call lgr%err('message', 'key')
   call lgr%err('message', 123)
   call lgr%err('message', 1.2e3)
   call lgr%err('message', 1.2d3)
   call lgr%err('message', 'key', 'op')
   call lgr%err('message', 'key', 'op', ['value'])
   call lgr%err('message', 'key', 'op', [123])
   call lgr%err('message', 'key', 'op', [1.2e3])
   call lgr%err('message', 'key', 'op', [1.2d3])
   call lgr%err('message', 'key', 'op', ['value 1', 'value 2'])
   call lgr%err('message', 'key', 'op', [123, 234])
   call lgr%err('message', 'key', 'op', [1.2e3, 2.3e4])
   call lgr%err('message', 'key', 'op', [1.2d3, 2.3d4])
   call lgr%end_section

   ! To see the output set failed to .true.
   failed = .false.
end function logging_err_test
