logical function logging_warn_test() result(failed)
   use logging

   implicit none

   type(logging_t) :: lgr

   call lgr%init('warn')
   call lgr%begin_section('test')
   call lgr%warn('message')
   call lgr%warn('message', 'key')
   call lgr%warn('message', 123)
   call lgr%warn('message', 1.2e3)
   call lgr%warn('message', 1.2d3)
   call lgr%warn('message', 'key', 'op')
   call lgr%warn('message', 'key', 'op', ['value'])
   call lgr%warn('message', 'key', 'op', [123])
   call lgr%warn('message', 'key', 'op', [1.2e3])
   call lgr%warn('message', 'key', 'op', [1.2d3])
   call lgr%warn('message', 'key', 'op', ['value 1', 'value 2'])
   call lgr%warn('message', 'key', 'op', [123, 234])
   call lgr%warn('message', 'key', 'op', [1.2e3, 2.3e4])
   call lgr%warn('message', 'key', 'op', [1.2d3, 2.3d4])
   call lgr%end_section

   ! To see the output set failed to .true.
   failed = .false.
end function logging_warn_test
