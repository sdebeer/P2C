!-------------
! Given an array with flagged cell perform clustering and return a list
! of boxes for refinement. Clustering is performed with the Berger-Rigoutsos
! algorithm
!
! AUTHOR: SC
! last modif: 02/12/08
!
!--------------

MODULE AMR

   IMPLICIT NONE
   TYPE AMRBoxList_type
      INTEGER, DIMENSION(3) :: LE, RE
      REAL                  :: eff
   END TYPE AMRBoxList_type

   INTEGER, PUBLIC          :: CheckJumpDim(3)

   !PRIVATE !..the following are private variables

   INTEGER, PRIVATE              :: prob_domain(3), LastId
   INTEGER, PRIVATE              :: MaxNSubGrids, MinNCells
   INTEGER, PRIVATE, SAVE        :: c_init
   REAL, PRIVATE                :: ClustEff
   TYPE BoxList_type
      INTEGER :: id, RE(3), LE(3)
      REAL    :: eff
      TYPE(BoxList_type), POINTER :: Next => NULL(), Prev => NULL()
   END TYPE BoxList_type
   TYPE(BoxList_type), ALLOCATABLE, TARGET, PRIVATE :: BoxList(:)
   TYPE Cutters
      INTEGER, ALLOCATABLE        :: sig(:), lap(:)
   END TYPE Cutters
   TYPE(Cutters), TARGET, PRIVATE :: coord(3)
   INTEGER, PRIVATE               :: Verbosity
   LOGICAL, PRIVATE               :: randomize
   LOGICAL, PRIVATE, SAVE         :: AMRinit_ = .false.

CONTAINS

!----- PROCEDURES ----------------------

   !------------------------------------------
   ! Default Constructor
   !-------------------------------------------
   SUBROUTINE AMRinit

      IMPLICIT NONE

      !..set default values
      AMRinit_ = .true.
      ClustEff = .75
      MaxNSubGrids = 100000
      Verbosity = 1  !..default: minimum information, only when problems arise
      MinNCells = 1
      c_init = 0
      randomize = .true.

      ALLOCATE (BoxList(0:MaxNSubGrids))

   END SUBROUTINE AMRINIT

   !------------------------------------------
   ! Default Destructor
   !------------------------------------------
   SUBROUTINE AMRclose

      IMPLICIT NONE

      IF (ALLOCATED(BoxList)) DEALLOCATE (BoxList)
      AMRinit_ = .false.

   END SUBROUTINE AMRclose

   !--- OTHER constructors
   SUBROUTINE AMRSetClustEff(NewClustEff)

      IMPLICIT NONE
      REAL, INTENT(IN) :: NewClustEff

      IF (.not. AMRinit_) STOP "AMR not initialized!"
      ClustEff = NewClustEff

   END SUBROUTINE AMRSetClustEff

   SUBROUTINE AMRSetVerbosity(NewVerbosity)

      IMPLICIT NONE
      INTEGER, INTENT(IN) :: NewVerbosity

      IF (.not. AMRinit_) STOP "AMR not initialized!"
      Verbosity = NewVerbosity

   END SUBROUTINE AMRSetVerbosity

   SUBROUTINE AMRSetMax(NewMaxNSubGrids)

      IMPLICIT NONE
      INTEGER, INTENT(IN) :: NewMaxNSubGrids

      IF (.not. AMRinit_) STOP "AMR not initialized"
      DEALLOCATE (BoxList)
      MaxNSubGrids = NewMaxNSubGrids
      ALLOCATE (BoxList(0:MaxNSubGrids))

   END SUBROUTINE AMRSetMax

   SUBROUTINE AMRSetMinNCells(NewMinNCells)

      IMPLICIT NONE
      INTEGER, INTENT(IN) :: NewMinNCells

      IF (.not. AMRinit_) STOP "AMR not initialized"

      IF (NewMinNCells <= 0) STOP "MinNCells must be >0!"

      !IF(MOD(NewMinNCells,2)==0) THEN
      !   NewMinNCells=NewMinNCells+1
      !   print *, "MinNCells will be changed to ",NewMinCells
      !END IF

      MinNCells = NewMinNCells

   END SUBROUTINE AMRSetMinNCells

   SUBROUTINE AMRSetRandomize(NewRandomize)

      IMPLICIT NONE
      LOGICAL, INTENT(IN) :: NewRandomize

      IF (.not. AMRinit_) STOP "AMR not initialized"

      randomize = NewRandomize

   END SUBROUTINE AMRSetRandomize

   SUBROUTINE AMRClustering(flag, SubGrids)

      IMPLICIT NONE
      INTEGER, INTENT(IN), DIMENSION(:, :, :) :: flag
      !REAL, INTENT(IN)                      :: ClustEff
      TYPE(AMRBoxList_type), INTENT(OUT), ALLOCATABLE :: SubGrids(:)
      INTEGER  :: nx, ny, nz, i, ii, count
      TYPE(BoxList_type), POINTER :: Curr

      IF (.not. AMRinit_) STOP "AMR not initialized!"

      IF (MAXVAL(flag) == 0) THEN
         IF (Verbosity >= 1) print *, "no refinement whitin this grid!"
         RETURN
      END IF

      nx = SIZE(flag, DIM=1); ny = SIZE(flag, DIM=2); nz = SIZE(flag, DIM=3)
      prob_domain = [nx, ny, nz]

      ALLOCATE (coord(1)%sig(nx), coord(2)%sig(ny), coord(3)%sig(nz))
      ALLOCATE (coord(1)%lap(nx), coord(2)%lap(ny), coord(3)%lap(nz))
      BoxList(0)%id = 0
      BoxList(0)%LE = 1
      BoxList(0)%RE = prob_domain

      !..compute signature of the domain
      Curr => BoxList(0)
      DO i = 1, 3
         SELECT CASE (i)
         CASE (1)
            DO ii = Curr%LE(i), Curr%RE(i)
               coord(i)%sig(ii) = SUM(flag(ii, Curr%LE(2):Curr%RE(2), &
                                           Curr%LE(3):Curr%RE(3)))
            END DO
         CASE (2)
            DO ii = Curr%LE(i), Curr%RE(i)
               coord(i)%sig(ii) = SUM(flag(Curr%LE(1):Curr%RE(1), ii, &
                                           Curr%LE(3):Curr%RE(3)))
            END DO
         CASE (3)
            DO ii = Curr%LE(i), Curr%RE(i)
               coord(i)%sig(ii) = SUM(flag(Curr%LE(1):Curr%RE(1), &
                                           Curr%LE(2):Curr%RE(2), ii))
            END DO
         END SELECT
         !print *, sig(i)%sig(Curr%LE(i):Curr%RE(i))
      END DO

      !..set the first guess with a simple scan
      DO i = 1, 3
         ii = 1
         DO WHILE (coord(i)%sig(ii) == 0)
            ii = ii + 1
         END DO
         BoxList(1)%LE(i) = ii
         ii = prob_domain(i)
         DO WHILE (coord(i)%sig(ii) == 0)
            ii = ii - 1
         END DO
         BoxList(1)%RE(i) = ii
      END DO
      BoxList(1)%Id = 1
      !..set pointer in the linked list
      BoxList(0)%Next => BoxList(1)
      BoxList(1)%Prev => BoxList(0)
      LastId = 1

      !..start recursive procedure for clustering
      Curr => BoxList(0)
      CALL BRClustering(flag, Curr)

      !..check list
      IF (ASSOCIATED(BoxList(0)%Next)) THEN
         Curr => BoxList(0)%Next
      ELSE
         IF (Verbosity >= 1) print *, "no boxes found!"
         RETURN
      END IF

      !..count subgrids
      count = 1
      count_loop: DO
         !print ('("BoxId=", i3, "  LE=",3(i3,1x), "  RE=",3(i3,1x),"  eff",f10.4)'), &
         !     Curr%Id, Curr%LE, Curr%RE, Curr%eff
         IF (ASSOCIATED(Curr%Next)) THEN
            count = count + 1
            Curr => Curr%Next
         ELSE
            EXIT count_loop
         ENDIF
      END DO count_loop

      IF (Verbosity >= 2) print *, "found n=", count, "subgrids"
      IF (ALLOCATED(SubGrids)) DEALLOCATE (SubGrids)
      ALLOCATE (SubGrids(count))
      Curr => BoxList(0)%Next
      !SubGrids(1)%LE==Curr%LE,Curr%RE,Curr%eff)
      SubGrids(1)%LE = Curr%LE
      SubGrids(1)%RE = Curr%RE
      SubGrids(1)%eff = Curr%eff
      count = 1
      io_loop: DO
         IF (ASSOCIATED(Curr%Next)) THEN
            count = count + 1
            Curr => Curr%Next
            !SubGrids(count)=[Curr%LE,Curr%RE,Curr%eff]
            SubGrids(count)%LE = Curr%LE
            SubGrids(count)%RE = Curr%RE
            SubGrids(count)%eff = Curr%eff
         ELSE
            EXIT io_loop
         ENDIF
      END DO io_loop

      DEALLOCATE (coord(1)%sig, coord(2)%sig, coord(3)%sig)
      DEALLOCATE (coord(1)%lap, coord(2)%lap, coord(3)%lap)

      RETURN

      !print *, "end of Job :)"

   END SUBROUTINE AMRClustering

!--------------------------

   SUBROUTINE BRClustering(flag, Curr)

      IMPLICIT NONE
      INTEGER, INTENT(IN), DIMENSION(:, :, :) :: flag
      TYPE(BoxList_type), POINTER :: Curr, This
      !REAL, INTENT(IN)            :: ClustEff
      REAL                        :: ThisEff, R
      INTEGER                     :: i, ii, cut, ThisJump, LargestJump, LargestJumpDim, LargestJumpCell, c
      INTEGER, POINTER, DIMENSION(:) :: checkval
      LOGICAL                     :: nextcycle

      cluster_loop: DO

         !..move to the next grid, if present
         IF (ASSOCIATED(Curr%Next)) THEN
            Curr => Curr%Next
         ELSE
            EXIT cluster_loop
         END IF

         !..calculate efficiency of Curr
         ThisEff = SUM(flag(Curr%LE(1):Curr%RE(1), &
                            Curr%LE(2):Curr%RE(2), &
                            Curr%LE(3):Curr%RE(3)))/ &
                   REAL(PRODUCT(Curr%RE(:) - Curr%LE(:) + 1))

         IF (Verbosity >= 2) print *, "---------------------------------------------------"
         IF (Verbosity >= 2) print *, "Box", Curr%Id, "of", LastId, "efficiency=", ThisEff
         Curr%eff = ThisEff

         IF (ThisEff == 0.) THEN !..this box does not contain flagged cell. To be disconnected from tree
            IF (Curr%Id == LastId) THEN
               This => Curr%Prev
               BoxList(This%Id)%Next => NULL()
               IF (Verbosity >= 2) print *, "LAST ONE!"
               EXIT cluster_loop
            ELSE
               IF (Verbosity >= 2) print *, "disconneting", Curr%Id, "from tree"
               This => Curr%Prev
               IF (Verbosity >= 2) print *, "now box", This%Id
               BoxList(This%Id)%Next => Curr%Next
               This => Curr%Next
               IF (Verbosity >= 2) print *, "points to", This%Id
               BoxList(This%Id)%Prev => Curr%Prev
            END IF
            Curr => Curr%Prev
            CYCLE cluster_loop
         END IF

         IF (ThisEff >= ClustEff) THEN
            IF (Curr%Id == LastId) THEN
               BoxList(LastId)%Next => NULL()
               IF (Verbosity >= 2) print *, "LAST ONE!"
               EXIT cluster_loop     !..job finished!
            ELSE
               CYCLE cluster_loop
            END IF
         END IF

         !..compute signatures
         DO i = 1, 3
            SELECT CASE (i)
            CASE (1)
               DO ii = Curr%LE(i), Curr%RE(i)
                  coord(i)%sig(ii) = SUM(flag(ii, Curr%LE(2):Curr%RE(2), &
                                              Curr%LE(3):Curr%RE(3)))
               END DO
            CASE (2)
               DO ii = Curr%LE(i), Curr%RE(i)
                  coord(i)%sig(ii) = SUM(flag(Curr%LE(1):Curr%RE(1), ii, &
                                              Curr%LE(3):Curr%RE(3)))
               END DO
            CASE (3)
               DO ii = Curr%LE(i), Curr%RE(i)
                  coord(i)%sig(ii) = SUM(flag(Curr%LE(1):Curr%RE(1), &
                                              Curr%LE(2):Curr%RE(2), ii))
               END DO
            END SELECT
            IF (Verbosity >= 2) print *, "signatures DIM=", i
            IF (Verbosity >= 2) print *, coord(i)%sig(Curr%LE(i):Curr%RE(i))
            !print *, sig(i)%val(:)
         END DO

         IF (randomize) THEN
            CALL RANDOM_NUMBER(R)
            c_init = INT(3*R)
         ENDIF

         nextcycle = .false.
         !..look for holes
         holes_loop: DO c = 0, 2, 1

            !..coordinates are in "randomized" order
            i = MOD(c_init + c, 3) + 1

            IF (Curr%RE(i) - Curr%LE(i) + 1 <= MinNCells) CYCLE holes_loop

            !checkval => coord(i)%sig(Curr%LE(i):Curr%RE(i))
            !IF(ANY(sig(i)%val(Curr%LE(i):Curr%RE(i)))==0) THEN ! hole is present
            IF (ANY(coord(i)%sig(Curr%LE(i):Curr%RE(i)) == 0)) THEN ! hole is present

               cut = Curr%LE(i) + 1 !..cutting edge

               IF (coord(i)%sig(Curr%LE(i)) == 0) THEN ! already in first position, scan for first non-zero

                  DO WHILE (coord(i)%sig(cut) == 0)
                     cut = cut + 1
                  END DO
                  cut = cut - 1

                  IF (cut - Curr%LE(i) + 1 >= MinNCells) THEN

                     CALL CreateBoxes(Curr, i, cut)

                     Curr => Curr%Prev
                     CYCLE cluster_loop

                  END IF

               ELSEIF (MinNCells > 1 .and. coord(i)%sig(Curr%LE(i) + MinNCells - 1) == 0) THEN

                  cut = cut + MinNCells - 1
                  DO WHILE (coord(i)%sig(cut) == 0 .and. cut < Curr%RE(i))
                     cut = cut + 1
                  END DO
                  IF (cut == Curr%RE(i)) THEN
                     cut = Curr%LE(i) + MinNCells - 1
                  ELSE
                     cut = cut - 1
                  END IF

                  CALL CreateBoxes(Curr, i, cut)

                  Curr => Curr%Prev
                  CYCLE cluster_loop

               ELSE !..scan for first zero

                  cut = cut + MinNCells - 1
                  DO WHILE (coord(i)%sig(cut) /= 0 .and. cut < Curr%RE(i))
                     cut = cut + 1
                  END DO
                  cut = cut - 1

                  IF (Curr%RE(i) - cut >= MinNCells) THEN

                     CALL CreateBoxes(Curr, i, cut)

                     Curr => Curr%Prev
                     CYCLE cluster_loop

                  END IF

               END IF
!             cut=cut-1

!             CALL CreateBoxes(Curr, i,cut)

!             nextcycle=.true.
!             EXIT holes_loop

            END IF

         END DO holes_loop

         !..update coordinates' "shuffler"
         IF (randomize) THEN
            CALL RANDOM_NUMBER(R)
            c_init = INT(3*R)
         ENDIF
         !c_init=2

!       IF(nextcycle) THEN
!          Curr => Curr%Prev
!          CYCLE cluster_loop
!       ENDIF

         IF (Verbosity >= 2) print *, "no holes, looking for inflection points..."

         !..compute laplacian
         LargestJump = 0
         LargestJumpDim = 0
         LargestJumpCell = 0
         DO c = 0, 2, 1

            !..coordinates are in "randomized" order, if option not deactivated
            i = MOD(c_init + c, 3) + 1

            IF (Curr%RE(i) - Curr%LE(i) + 1 < 2*MinNCells) CYCLE

            coord(i)%lap(Curr%LE(i)) = -2*coord(i)%sig(Curr%LE(i)) + coord(i)%sig(Curr%LE(i) + 1)
            coord(i)%lap(Curr%RE(i)) = coord(i)%sig(Curr%RE(i) - 1) - 2*coord(i)%sig(Curr%RE(i))

            !ELSE !..general case

            DO ii = Curr%LE(i) + 1, Curr%RE(i) - 1, 1
               coord(i)%lap(ii) = coord(i)%sig(ii - 1) - 2*coord(i)%sig(ii) + coord(i)%sig(ii + 1)
            END DO

            !..scan laplacial for inflection points
            DO ii = Curr%LE(i) + MinNCells - 1, Curr%RE(i) - MinNCells
               !..first scan zero-crossings
               IF (coord(i)%lap(ii)*coord(i)%lap(ii + 1) <= 0) THEN
                  ThisJump = abs(coord(i)%lap(ii)) + abs(coord(i)%lap(ii + 1))
                  IF (ThisJump > LargestJump) THEN
                     LargestJump = ThisJump
                     LargestJumpDim = i
                     LargestJumpCell = ii
                  END IF
               END IF
            END DO

            IF (Verbosity >= 2) THEN
               print *, "Laplacian DIM=", i
               print *, coord(i)%lap(Curr%LE(i):Curr%RE(i))
            END IF
            !print *, sig(i)%val(:)

         END DO

         IF (LargestJump == 0) THEN
            IF (Verbosity >= 2) print *, "Box", Curr%Id, "cannot be splitted better than this (no more zero-crossings)"
            IF (Curr%Id == LastId) THEN
               BoxList(LastId)%Next => NULL()
               IF (Verbosity >= 2) print *, "LAST ONE!"
               EXIT cluster_loop     !..job finished!
            ELSE
               CYCLE cluster_loop
            END IF
         END IF

         !print *, LargestJumpDim, LargestJumpCell
         IF (Verbosity >= 2) print *, "Inflection point: Dim=", LargestJumpDim, "CutEdge=", LargestJumpCell
         CALL CreateBoxes(Curr, LargestJumpDim, LargestJumpCell)

         checkJumpDim(LargestJumpDim) = checkJumpDim(LargestJumpDim) + 1

         Curr => Curr%Prev
         CYCLE cluster_loop

      END DO cluster_loop

      RETURN

   END SUBROUTINE BRClustering

   SUBROUTINE CreateBoxes(Curr, i, cut)

      IMPLICIT NONE
      INTEGER, INTENT(IN) :: i, cut
      TYPE(BoxList_type), POINTER :: Curr, This

      !..create two new boxes with cutting edge 'cut' on plane 'i' and append them at the end of linked list

      IF (LastId >= MaxNSubGrids - 1) THEN
         print *, "No more free memory: Increase MaxNSubGrids!"
         print *, "Old value=", MaxNSubGrids
         print *, "Use procedure AMRSetMax to increase this value"
         STOP
      END IF

      !..first box (left-box):
      BoxList(LastId + 1)%Id = LastId + 1
      BoxList(LastId + 1)%LE(:) = Curr%LE(:)
      !WHERE([1,2,3]/=i)
      BoxList(LastId + 1)%RE(:) = Curr%RE(:)
      !END WHERE
      BoxList(LastId + 1)%RE(i) = cut

      !..second box (right-box):
      BoxList(LastId + 2)%Id = LastId + 2
      BoxList(LastId + 2)%RE(:) = Curr%RE(:)
      !WHERE(ii/=i)
      BoxList(LastId + 2)%LE(:) = Curr%LE(:)
      !END WHERE
      BoxList(LastId + 2)%LE(i) = cut + 1

      !..update hierarchy:
      This => Curr%Prev
      IF (Curr%Id /= LastId) THEN
         BoxList(This%Id)%Next => Curr%Next !..disconnect Curr Box
         This => Curr%Next
         BoxList(This%Id)%Prev => Curr%Prev
         BoxList(LastId)%Next => BoxList(LastId + 1)
         BoxList(LastId + 1)%Prev => BoxList(LastId)
      ELSE
         BoxList(This%Id)%Next => BoxList(LastId + 1) !..disconnect Curr Box
         BoxList(LastId + 1)%Prev => BoxList(This%Id)
      END IF
      BoxList(LastId + 1)%Next => BoxList(LastId + 2)
      BoxList(LastId + 2)%Prev => BoxList(LastId + 1)
      LastId = LastId + 2

      IF (Verbosity >= 2) THEN
         print *, "creating:"
         print *, "BoxLeft:", BoxList(LastId - 1)%Id, ":"
         print *, BoxList(LastId - 1)%LE, BoxList(LastId - 1)%RE
         This => BoxList(LastId - 1)%Next
         print *, "=>", This%Id
         This => BoxList(LastId - 1)%Prev
         print *, This%Id, "<="
         print *, "BoxRigth:", BoxList(LastId)%Id, ":"
         print *, BoxList(LastId)%LE, BoxList(LastId)%RE
         IF (ASSOCIATED(BoxList(LastId)%Next)) THEN
            This => BoxList(LastId)%Next
            print *, "=>", This%Id
         ELSE
            print *, "=> NULL()"
         END IF
         This => BoxList(LastId)%Prev
         print *, This%Id, "<="
      END IF

   END SUBROUTINE CreateBoxes

END MODULE AMR
