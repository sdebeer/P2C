submodule(logging) logo_smod
contains
module subroutine logging_set_colored_logo(this)
   implicit none

   class(logging_t), intent(inout) :: this

   this%colored_logo(1) = tc%nc
   this%colored_logo(2) = tc%rd//"f▄▄▄▄▄▄     ▄▄▄▄▄       ▄▄▄▄"//tc%nc
   this%colored_logo(3) = tc%gn//" ██▀▀▀▀█▄  █▀▀▀▀██▄   ██▀▀▀▀█"//tc%nc
   this%colored_logo(4) = tc%yl//" ██    ██        ██  ██▀"//tc%nc
   this%colored_logo(5) = tc%ig//" ██████▀       ▄█▀   ██"//tc%nc
   this%colored_logo(6) = tc%vt//" ██          ▄█▀     ██▄"//tc%nc
   this%colored_logo(7) = tc%bl//" ██        ▄██▄▄▄▄▄   ██▄▄▄▄█"//tc%nc
   this%colored_logo(8) = tc%rd//" ▀▀        ▀▀▀▀▀▀▀▀     ▀▀▀▀"//tc%nc
   this%colored_logo(9) = tc%nc
   this%colored_logo(10) = tc%nc
end subroutine logging_set_colored_logo

module subroutine logging_set_logo(this)
   implicit none

   class(logging_t), intent(inout) :: this

   this%logo(1) = ""
   this%logo(2) = "f▄▄▄▄▄▄     ▄▄▄▄▄       ▄▄▄▄"
   this%logo(3) = " ██▀▀▀▀█▄  █▀▀▀▀██▄   ██▀▀▀▀█"
   this%logo(4) = " ██    ██        ██  ██▀"
   this%logo(5) = " ██████▀       ▄█▀   ██"
   this%logo(6) = " ██          ▄█▀     ██▄"
   this%logo(7) = " ██        ▄██▄▄▄▄▄   ██▄▄▄▄█"
   this%logo(8) = " ▀▀        ▀▀▀▀▀▀▀▀     ▀▀▀▀"
   this%logo(9) = ""
   this%logo(10) = ""
end subroutine logging_set_logo
end submodule logo_smod

