submodule(plotter) canvas_clear_smod
contains
pure module subroutine plotter_canvas_clear(canvas)
   implicit none

   class(plotter_canvas_t), intent(inout) :: canvas

   canvas%grid = ''
end subroutine plotter_canvas_clear
end submodule canvas_clear_smod
